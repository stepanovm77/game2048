<html>
    <head>
        <meta charset="UTF-8">
        <title>Game 2048</title>
        <link href="/style.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="/game.js"></script>
    </head>
    <body>
        <h1>Game 2048</h1>
        <table class="board">
            <tr>
                <td class="tile" id="tile0"></td>
                <td class="tile" id="tile1"></td>
                <td class="tile" id="tile2"></td>
                <td class="tile" id="tile3"></td>
            </tr>
            <tr>
                <td class="tile" id="tile4"></td>
                <td class="tile" id="tile5"></td>
                <td class="tile" id="tile6"></td>
                <td class="tile" id="tile7"></td>
            </tr>
            <tr>
                <td class="tile" id="tile8"></td>
                <td class="tile" id="tile9"></td>
                <td class="tile" id="tile10"></td>
                <td class="tile" id="tile11"></td>
            </tr>
            <tr>
                <td class="tile" id="tile12"></td>
                <td class="tile" id="tile13"></td>
                <td class="tile" id="tile14"></td>
                <td class="tile" id="tile15"></td>
            </tr>
        </table>
        <div id="analize"></div>
        <input type="button" value="goHeuristicsAnalize" onclick="goHeuristicsAnalize();" />
        <input type="button" value="goCompGame (s)" onclick="goCompGame();" />
        
        <a href="analize.php">анализ</a>
    </body>
</html>