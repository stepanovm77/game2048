var currentPosition;
var interval = 0;
var renderEachMove = true;

window.onload = function(){
    startGame();
    $(document).keydown(function(e){
        var keyCode = e.nexteyCode || e.which;
        //console.log(keyCode);
        if(keyCode === 37 || keyCode === 38 || keyCode === 39 || keyCode === 40){
            goTurn(keyCode);
        } else if (keyCode === 83) {
            // s
            if(interval === 0){
                interval = setInterval(function(){
                    goCompGame();
                }, 100);
            } else {
                clearInterval(interval);
                interval = 0;
            }
        } else if (keyCode === 68) {
            // d
            showPositionEval(currentPosition);
        }
    });
};

function startGame(){
    currentPosition = new Position();
    for(var i = 0; i < 16; i++){
        currentPosition.board[i] = 0;
    }
    currentPosition.addTile();
    currentPosition.addTile();
    redrawBoard();
}


function getMaxPosValue(position){
    var m = 0;
    for(var i = 0; i < 16; i++){
        if(position.board[i] > m){
            m = position.board[i];
        }
    }
    return m;
}

function goTurn(keyCode){
    moveAvailable = false;
    switch(keyCode){
        case 38:
            currentPosition = currentPosition.goUp();
            break;
        case 39:
            currentPosition = currentPosition.goRight();
            break;
        case 37:
            currentPosition = currentPosition.goLeft();
            break;
        case 40:
            currentPosition = currentPosition.goDown();
            break;
        default:
            break;
    }
    if(currentPosition.tilesMoveCounter > 0){
        currentPosition.addTile();
        if(renderEachMove){
            redrawBoard();
        }
    }
}

function redrawBoard(){
    for(var i = 0; i < 16; i++){
        var tileBlock = document.getElementById('tile'+i);
        tileBlock.innerHTML = (currentPosition.board[i] !== 0) ? currentPosition.board[i] : '';
        $(tileBlock).css('background-color', bgColors[currentPosition.board[i]]);
        if(currentPosition.board[i] > 4000){ $(tileBlock).css('color', '#fff'); } else { $(tileBlock).css('color', '#000'); }
    }
}


function Position(){
    this.board = [];
    this.blockedTiles = [];
    this.tilesMoveCounter = 0;
    
    this.clone = function(){
        var newPosition = new Position();
        newPosition.board = this.board.slice();
        return newPosition;
    };
    
    this.getMaxPosValue = function(){
        var m = 0;
        for(var i = 0; i < 16; i++){
            if(this.board[i] > m){
                m = this.board[i];
            }
        }
        return m;
    };
    
    this.addTile = function(){
        var emptyTiles = this.getEmptyTiles();
        var newTilePosition = emptyTiles[randomInteger(0, emptyTiles.length - 1)];
        var newTileVal = randomInteger(0, 9);
        newTileVal = (newTileVal === 0) ? 4 : 2;
        this.board[newTilePosition] = newTileVal;
    };
    
    // return empty tiles indexes
    this.getEmptyTiles = function(){
        var emptyTiles = [];
        for(var i = 0; i < 16; i++){
            if(this.board[i] === 0){
                emptyTiles.push(i);
            }
        }
        return emptyTiles;
    };
    
    
    this.goUp = function(){
        var newPosition = this.clone();
        for(var i = 4; i < 16; i++){
            if(newPosition.board[i] > 0){
                newPosition.moveTileUp(i);
            }
        }
        return newPosition;
    };
    
    this.goDown = function(){
        var newPosition = this.clone();
        for(var i = 2; i >= 0; i--){
            for(var j = 0; j < 4; j++){
                var k = j + i*4;
                if(newPosition.board[k] > 0){
                    newPosition.moveTileDown(k);
                }
            }
        }
        return newPosition;
    };
    
    this.goRight = function(){
        var newPosition = this.clone();
        for(var i = 2; i >= 0; i--){
            for(var j = 0; j < 4; j++){
                var k = j*4 + i;
                if(newPosition.board[k] > 0){
                    newPosition.moveTileRight(k);
                }
            }
        }
        return newPosition;
    };
    
    this.goLeft = function(){
        var newPosition = this.clone();
        for(var i = 1; i < 4; i++){
            for(var j = 0; j < 4; j++){
                var k = j*4 + i;
                if(newPosition.board[k] > 0){
                    newPosition.moveTileLeft(k);
                }
            }        
        }
        return newPosition;
    };
    
    
    
    this.moveTileUp = function(pos){
        if(pos < 4){ return; }
        var moveToPos = pos - 4;
        this.moveTile(pos, moveToPos, 'up');
    };    
    this.moveTileDown = function(pos){
        if(pos > 11){ return; }
        var moveToPos = pos + 4;
        this.moveTile(pos, moveToPos, 'down');
    };
    this.moveTileRight = function(pos){
        if((pos + 1) % 4 === 0){ return; }
        var moveToPos = pos + 1;    
        this.moveTile(pos, moveToPos, 'right');    
    };
    this.moveTileLeft = function(pos){
        if((pos + 4) % 4 === 0){ return; }
        var moveToPos = pos - 1;    
        this.moveTile(pos, moveToPos, 'left');
    };
    
    
    this.moveTile = function(start, end, way){
        if(this.board[end] === 0){
            // если пусто:
            this.board[end] = this.board[start];
            this.board[start] = 0;
            switch(way){
                case 'up': this.moveTileUp(end); break;
                case 'right': this.moveTileRight(end); break;
                case 'left': this.moveTileLeft(end); break;
                case 'down': this.moveTileDown(end); break;
                default: break;                
            }
            this.tilesMoveCounter++;
        } else {
            // если числа совпадают:
            if(this.board[end] === this.board[start] && this.blockedTiles.indexOf(end) < 0){
                this.board[end] = this.board[end] * 2;
                this.blockedTiles.push(end);
                this.board[start] = 0;
                this.tilesMoveCounter++;
            }
        }
    };
    
}










var bgColors = {
    0: '#fff',
    2: '#eee4da',
    4: '#ece0ca',
    8: '#f2b17b',
    16: '#f59565',
    32: '#f57c5f',
    64: '#f95b3a',
    128: '#edce72',
    256: '#edcc60',
    512: '#edcc60',
    1024: '#edcc60',
    2048: '#edcc60',
    4096: '#36aa42',
    8192: '#3d3a33',
    16384: '#3d3a33',
    32768: '#3d3a33'   
};

function randomInteger(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1);
    rand = Math.round(rand);
    return rand;
}




var analizePositionsCounter = 0;
var finalEvals_up = [];
var finalEvals_down = [];
var finalEvals_right = [];
var finalEvals_left = [];

var gameover = false;

function autoGame(){
    renderEachMove = false;
    while(gameover === false){
        if(goAI()){
            goTurn(getMove());
        } else {
            gameover = true;
            alert('Game Over');            
            redrawBoard();
        }
    }
    
}


function goCompGame(){
    
    if(goAI()){
        goTurn(getMove());
    } else {
        alert('Game Over');
        clearInterval(interval);
        interval = 0;
    }
    
}

function goAI(){
    var slidePos;
    var newPositionInfo;
    var aiMoveAvailable = false;
    
    finalEvals_up = [];
    finalEvals_down = [];
    finalEvals_right = [];
    finalEvals_left = [];
    analizePositionsCounter = 0;
    
    slidePos = currentPosition.goDown();
    if(slidePos.tilesMoveCounter > 0){
        //$('#analize').append('<p>__________________ goDown _________________</p>');
        aiMoveAvailable = true;
        newPositionInfo = new PositionInfo(slidePos, 1 ); // создаем новый PositionInfo
        analyzeMove(newPositionInfo, 0, 'down');
    }
    
    slidePos = currentPosition.goUp();
    if(slidePos.tilesMoveCounter > 0){
        //$('#analize').append('<p>__________________ goUp _________________</p>');
        aiMoveAvailable = true;
        newPositionInfo = new PositionInfo(slidePos, 1 ); // создаем новый PositionInfo
        analyzeMove(newPositionInfo, 0, 'up');
    }
    
    slidePos = currentPosition.goRight();
    if(slidePos.tilesMoveCounter > 0){
        //$('#analize').append('<p>__________________ goRight _________________</p>');
        aiMoveAvailable = true;
        newPositionInfo = new PositionInfo(slidePos, 1 ); // создаем новый PositionInfo
        analyzeMove(newPositionInfo, 0, 'right');
    }
    
    slidePos = currentPosition.goLeft();
    if(slidePos.tilesMoveCounter > 0){
        //$('#analize').append('<p>__________________ goLeft _________________</p>');
        aiMoveAvailable = true;
        newPositionInfo = new PositionInfo(slidePos, 1 ); // создаем новый PositionInfo
        analyzeMove(newPositionInfo, 0, 'left');
    }
    
    
    
    return aiMoveAvailable;
    
}

function getMove(){
    var turnCode = 0, tmpMove, bestMove = 0;
    tmpMove = getMidAmount(finalEvals_up);
    if(tmpMove > bestMove){ bestMove = tmpMove; turnCode = 38; }
    //$('#analize').append('<p>finalEvals_up mid eval === '+tmpMove+'</p>');
    
    tmpMove = getMidAmount(finalEvals_down);
    if(tmpMove > bestMove){ bestMove = tmpMove; turnCode = 40; }
    //$('#analize').append('<p>finalEvals_down mid eval === '+tmpMove+'</p>');
    
    tmpMove = getMidAmount(finalEvals_left);
    if(tmpMove > bestMove){ bestMove = tmpMove; turnCode = 37; }
    //$('#analize').append('<p>finalEvals_left mid eval === '+tmpMove+'</p>');
    
    tmpMove = getMidAmount(finalEvals_right);
    if(tmpMove > bestMove){ bestMove = tmpMove; turnCode = 39; }
    //$('#analize').append('<p>finalEvals_right mid eval === '+tmpMove+'</p>');
    
    
    //$('#analize').append('<p>turnCode choosen: </p>'+turnCode);
    return turnCode;
}

function getMidAmount(arr){
    var r = 0;
    if(arr.length === 0){
        return 0;
    }
    for(var i = 0; i < arr.length; i++){
        r += arr[i];
    }
    r /= arr.length;
    return r;
}

function PositionInfo(position, probability){
    this.position = position;
    this.probability = probability;
}

function analyzeMove(positionInfo, deep, mainMoveType){
    analizePositionsCounter++;
    var newPositions = [];
    var currPos = positionInfo.position;
    // получить все варианты нового расположения
    
    // получим свободные индексы:
    var emptyTiles = currPos.getEmptyTiles();
    // бежим по ним:
    for(var i = 0; i < emptyTiles.length; i++){
        var newPosition;
        var probability;
        // обработаем появление 2 в пустом тайле:
        // создаем новую позицию:
        newPosition = currPos.clone();
        // записываем тайл 2:
        newPosition.board[emptyTiles[i]] = 2;
        // посчитаем вероятноть этой позиции:
        probability = 0.9 * positionInfo.probability;
        // сохраним позицию в массиве:
        newPositions.push(new PositionInfo(newPosition, probability));
        
        // сделаем то же для тайла 4:
        newPosition = currPos.clone();
        newPosition.board[emptyTiles[i]] = 4;
        probability = 0.1 * positionInfo.probability;
        newPositions.push(new PositionInfo(newPosition, probability));
    }
    
    // нужно ли делать дальше?
    if(deep < AIdeep){
        for(var i = 0; i < newPositions.length; i++){
            var slidePos;
            var newPositionInfo;
            // каждую позицию сдвинем во всех направлениях:
            // верх:
            slidePos = newPositions[i].position.goUp(); // сдвигаем позицию вверх
            if(slidePos.tilesMoveCounter > 0){
                // если перемешение было осуществлено:
                newPositionInfo = new PositionInfo(slidePos, newPositions[i].probability ); // создаем новый PositionInfo
                analyzeMove(newPositionInfo, ++deep, mainMoveType); // рекурсивно запускаем анализ позиции.
            } else {
                // сюда попали, если не получилось сделать ход.
                // оцениваем позицию, добавляем ее к массиву оценок хода.
                /*
                for(var d = deep; d < AIdeep; d++){
                    // добавляем вероятности, чтобы оценка была в плоскости остальных оценок позиций, которые достигли нужной глубины.
                    newPositions[i].probability *= 0.9;
                }
                evalPosition(newPositions[i], mainMoveType);
                */
                finalEvals_up.push(0);
            }
            
            // то же делаем для остальных направлений:
            slidePos = newPositions[i].position.goDown();
            if(slidePos.tilesMoveCounter > 0){
                newPositionInfo = new PositionInfo(slidePos, newPositions[i].probability ); // создаем новый PositionInfo
                analyzeMove(newPositionInfo, ++deep, mainMoveType); // рекурсивно запускаем анализ позиции.
            } else {
                finalEvals_down.push(0);
            }
            slidePos = newPositions[i].position.goRight();
            if(slidePos.tilesMoveCounter > 0){
                newPositionInfo = new PositionInfo(slidePos, newPositions[i].probability ); // создаем новый PositionInfo
                analyzeMove(newPositionInfo, ++deep, mainMoveType); // рекурсивно запускаем анализ позиции.
            } else {
                finalEvals_right.push(0);
            }
            slidePos = newPositions[i].position.goLeft();
            if(slidePos.tilesMoveCounter > 0){
                newPositionInfo = new PositionInfo(slidePos, newPositions[i].probability ); // создаем новый PositionInfo
                analyzeMove(newPositionInfo, ++deep, mainMoveType); // рекурсивно запускаем анализ позиции.
            } else {
                finalEvals_left.push(0);
            }
            
        }
    } else {
        // Достигли нужной глубины. Оценим все полученные позиции и запишем их оценки:
        for(var i = 0; i < newPositions.length; i++){
            evalPosition(newPositions[i], mainMoveType);
        }
    }
    
}


var analizeResults = [];
var analizeCounter = 0;


function goHeuristicsAnalize(){
    
    // позиция, на которой остановились.
    var aa = 1, bb = 1, cc = 1, dd = 1, ee = 7;
    if(analizeResults.length > 0){
        var key = analizeResults.length - 1;
        aa = analizeResults[key].heuristics.a;
        bb = analizeResults[key].heuristics.b;
        cc = analizeResults[key].heuristics.c;
        dd = analizeResults[key].heuristics.d;
        ee = analizeResults[key].heuristics.e;
        if(ee === 10){
            ee = 1;
            if(dd === 3){
                dd = 1;
                if(cc === 3){
                    bb = 1;
                    if(aa === 3){
                        console.log('HeuristicsAnalize finished...');
                        return;
                    } else {
                        aa++;
                    }
                } else {
                    cc++;
                }
            } else {
                dd++;
            }
        } else {
            ee++;
        }
        console.log('HeuristicsAnalize continue with ...');
        console.log(analizeResults[key].heuristics);
    } else {
        console.log('HeuristicsAnalize started...');
    }
    
    var a = aa, b = bb, c = cc, d = dd, e = ee;     // устанавливаем счетчики циклов в последнюю позицию.
    var bs = 1, cs = 1, ds = 1, es = 7;             // начальные значения счетчиков.
    
    for(a; a < 4; a++){
        if(a !== aa) b = bs;
        for(b; b < 4; b++){
            if(b !== bb) c = cs;
            for(c; c < 4; c++){
                if(c !== cc) d = ds;
                for(d; d < 4; d++){
                    if(d !== dd) e = es;
                    for(e; e < 11; e++){
                        for(var i = 0; i < 2; i++){                       
                            emptyWeight = a;
                            maxTileWeight = b;
                            maxTileInEdgeWeight = c;
                            megresWeight = d;
                            monotonicityWeight = e;
                            
                            startGame();
                            heuristicsAnalize_goGame();
                            analizeResults.push( {
                                heuristics: {a: a, b: b, c: c, d: d, e: e}, 
                                position: currentPosition.board,
                                max: getMaxPosValue(currentPosition)
                            } );
                        }
                        analizeCounter++;
                        console.log(analizeResults[analizeResults.length - 1].heuristics);
                        if(analizeCounter % 10 === 0){
                            console.log('HeuristicsAnalize paused. Please, press start button again...');
                            return;
                        }
                    }
                }
            }
        }
    }
    console.log('HeuristicsAnalize finished...');
}

function heuristicsAnalize_goGame(){
    gameover = false;
    renderEachMove = false;
    while(gameover === false){
        if(goAI()){
            goTurn(getMove());
        } else {
            gameover = true;
        }
    }
}

//analizeWeights(5,7,3,5,10,3);

function analizeWeights(a,b,c,d,e,f){
    emptyWeight = 5;
    maxTileWeight = 7;
    maxTileInEdgeWeight = 3;
    megresWeight = 5;
    monotonicityWeight = 10;
    largeTilesInEdgeWeight = 3;

    for(var i = 0; i < 15; i++){
        console.log('wait...'+i);
        startGame();
        heuristicsAnalize_goGame();
        analizeResults.push( {
            heuristics: {a: a, b: b, c: c, d: d, e: e, f: f}, 
            position: currentPosition.board,
            max: getMaxPosValue(currentPosition)
        } );
    }
    
    console.log(analizeResults);
}

//analizeWeights(5,5,1,5,50);
/*
 * 
 * position: Array(16), max: 2048}1: {heuristics: {…}, position: Array(16), max: 1024}2: {heuristics: {…}, position: Array(16), max: 2048}3: {heuristics: {…}, position: Array(16), max: 2048}length: 4__proto__: Array(0)

 */


// 4096::
//
//var AIdeep = 5;
//var emptyWeight = 5;
//var maxTileWeight = 7;
//var maxTileInEdgeWeight = 3;
//var megresWeight = 7;
//var monotonicityWeight = 10;
//var largeTilesInEdgeWeight = 3;


var AIdeep = 4;
var emptyWeight = 7;
var maxTileWeight = 5;
var maxTileInEdgeWeight = 3;
var megresWeight = 7;
var monotonicityWeight = 10;
var largeTilesInEdgeWeight = 3;

// currentPosition.board = [4, 0, 0, 0, 2, 2, 0, 0, 8, 16, 256, 0, 256, 4, 4, 4];


// оцениваем позицию, добавляем ее к массиву оценок хода.
function evalPosition(newPositionInfo, moveType, show){
    var show = (show) ? show : false;
    var eval = 0;
    var probability = newPositionInfo.probability;
    var probEval = 0;
    var position = newPositionInfo.position;
    var pMax = position.getMaxPosValue();
    
    var maxTileInEdge = heuristic_maxTileInEdge(pMax, position.board) * maxTileInEdgeWeight;
    var emptyCells = heuristic_emptyCells(position) * emptyWeight;
    var LargeTilesInEdge = heuristic_LargeTilesInEdge(position.board) * largeTilesInEdgeWeight;
    var maxTile = heuristic_maxTile(pMax) * maxTileWeight;
    var merges = heuristic_monotonicity(position).merges * megresWeight;    
    var monotonicity = heuristic_monotonicity(position).monotonicity * monotonicityWeight;
    
    eval = eval 
            - LargeTilesInEdge
            + emptyCells 
            + maxTile 
            + merges 
            + monotonicity 
            + maxTileInEdge
            ;
    probEval = parseInt(eval * probability);
    probEval = eval;
    
    switch(moveType){
        case 'up': finalEvals_up.push(probEval); break;
        case 'down': finalEvals_down.push(probEval); break;
        case 'right': finalEvals_right.push(probEval); break;
        case 'left': finalEvals_left.push(probEval); break;
        default: break;
    }
    
    if(show){
        $('#analize').append('<p>_____________________________________</p>');    
        for(var i = 0; i < 16; i++){
            if(i % 4 === 0){
                $('#analize').append('<br />');
            }
            $('#analize').append(position.board[i]+', '); 
        }    
        $('#analize').append('<p>emptyCells = '+emptyCells+'</p>');
        $('#analize').append('<p>maxTileInEdge = '+maxTileInEdge+'</p>');
        $('#analize').append('<p>LargeTilesInEdge = '+LargeTilesInEdge+'</p>');
        $('#analize').append('<p>maxTile = '+maxTile+'</p>');
        $('#analize').append('<p>merges = '+merges+'</p>');
        $('#analize').append('<p>monotonicity = '+monotonicity+'</p>');
        $('#analize').append('<p>eval = '+eval+'</p>');
        $('#analize').append('<p>probEval = '+probEval+'</p>');
    }
}

function showPositionEval(position){
    evalPosition(new PositionInfo(position, 1), 'up', true);
}


    

function heuristic_maxTileInEdge(pMax, board){
    if(board[0] === pMax
            || board[3] === pMax
            || board[12] === pMax
            || board[15] === pMax){
        return 100;
    }
    return 0;
}

function heuristic_LargeTilesInEdge(board){
    var res = 0;
    for(var i = 5; i <= 10; i++){
        if(board[i] > 0 && (i === 5 || i === 6 || i === 9 || i === 10)){
            // если элемент в центре
            // приводим значения к 0-100
            var r = board[i] * 0.104;
            r = (r > 53) ? 53 : r;
            res += r;
        }
    }
    res = (res > 100) ? 100 : res;
    return res;
}


// return value between 0 and 1, 1 - good;
function heuristic_emptyCells(pos){
    var res;
    var emptyCount = pos.getEmptyTiles().length;
    if(emptyCount === 0){
        return 0;
    }
    res = Math.log(pos.getEmptyTiles().length); // получили диапазон 0 - 2.7
    res *= 37;  // переведем к 0 - 100:
    return res;
}

function heuristic_maxTile(pMax){
    var res = Math.log2(pMax); // получили диапазон 1-16
    res *= 6.25;    // переведем к 0 - 100:
    return res;
}

// return value between 0 and 1, where 1 - best board;
function heuristic_monotonicity2(pos){
    // окей, пробуем так:
    /*
     * алгоритм перебирает все строки и столбцы.
     * в каждом записывает разницу между парами.
     * если строка/столбец не монотонны, складываем разницы.
     * число делим на 100 и берем его логарифм с основанием 2.
     * таким образом в оценку попадут столбцы, сумма "проблемы" которого будет больше 100 примерно.
     */
    
    var finalRes = 0;
    var megres = 0;
    var pos = pos.board;
    
    for(var i = 0; i < 4; i++){
        var tmp;
        var eval_array = [0,0,0,0];
        var row_cells = [];
        var col_cells = [];
        // записываем значения, игнорируя пустые строки:
        for(j = 0; j < 4; j++){
            if(pos[j + i*4] !== 0){
                row_cells.push(pos[j + i*4]);
            }
            if(pos[j*4 + i] !== 0){
                col_cells.push(pos[j*4 + i]);
            }
        }
        // строки:
        if(row_cells.length > 2){
            for(var k = 0; k < row_cells.length - 1; k++){
                if(row_cells[k] > row_cells[k + 1]){
                    eval_array[0] += row_cells[k] - row_cells[k + 1];
                } 
                if(row_cells[k] < row_cells[k + 1]){
                    eval_array[1] += row_cells[k + 1] - row_cells[k];
                }
                if(row_cells[k] === row_cells[k + 1]){
                    megres++;
                }
            }
            // сложим полученные суммы.
            tmp = eval_array[0] + eval_array[1];
            // если сумма равна одному из слагаемых, значит, второе 0 и монотонность есть.
            // в противному случае добавляем сумму к итоговому результату.
            if(tmp > 20 && !(eval_array[0] === 0 || eval_array[1] === 0)){
                tmp = (tmp < 100) ? 101 : tmp;
                finalRes += Math.log2(tmp / 100);
            }
        }
        
        // столбцы
        if(col_cells.length > 2){
            for(var k = 0; k < col_cells.length - 1; k++){
                if(col_cells[k] > col_cells[k + 1]){
                    eval_array[2] += col_cells[k] - col_cells[k + 1];
                } 
                if(col_cells[k] < col_cells[k + 1]){
                    eval_array[3] += col_cells[k + 1] - col_cells[k];
                }
                if(col_cells[k] === col_cells[k + 1]){
                    megres++;
                }
            }
            tmp = eval_array[2] + eval_array[3];
            if(tmp > 20 && !(eval_array[2] === 0 || eval_array[3] === 0)){
                tmp = (tmp < 100) ? 101 : tmp;
                finalRes += Math.log2(tmp / 100);
            }
        }
    }
    
    finalRes = (finalRes === 0) ? 1 : 1 - (finalRes * monotonicityWeight / 100);
    megres *= megresWeight;
    return { monotonicity: finalRes, merges: megres };
}




// return value between 0 and 1, where 1 - best board;
function heuristic_monotonicity(pos){
    // окей, пробуем так:
    /*
     * алгоритм перебирает все строки и столбцы.
     * в каждом записывает разницу между парами.
     * если строка/столбец не монотонны, складываем разницы.
     * число делим на 100 и берем его логарифм с основанием 2.
     * таким образом в оценку попадут столбцы, сумма "проблемы" которого будет больше 100 примерно.
     */
    
    var finalRes = 0;
    var megres = 0;
    var pos = pos.board;
    
    for(var i = 0; i < 4; i++){
        var tmp;
        var eval_array = [0,0,0,0];
        var row_cells = [];
        var col_cells = [];
        // записываем значения, игнорируя пустые строки:
        for(j = 0; j < 4; j++){
            if(pos[j + i*4] !== 0){
                row_cells.push(pos[j + i*4]);
            }
            if(pos[j*4 + i] !== 0){
                col_cells.push(pos[j*4 + i]);
            }
        }
        // строки:
        if(row_cells.length > 2){
            for(var k = 0; k < row_cells.length - 1; k++){
                if(row_cells[k] > row_cells[k + 1]){
                    eval_array[0] += row_cells[k] - row_cells[k + 1];
                } 
                if(row_cells[k] < row_cells[k + 1]){
                    eval_array[1] += row_cells[k + 1] - row_cells[k];
                }
                if(row_cells[k] === row_cells[k + 1]){
                    megres++;
                }
            }
            
            // если есть немонотонность, вычислим ее.
            // перемножаем суммы. делим результат на 1000. берем его логарифм с основанием 2.
            // таким образом получим значения в диапазоне [-8;20]
            // значимые немонотонности начинаются с 0. 
            // ограничим все варианты диапазоном [1;10]
            if(!(eval_array[0] === 0 || eval_array[1] === 0)){
                tmp = Math.log2((eval_array[0] * eval_array[1]) / 1000);
                tmp = (tmp < 1) ? 1 : tmp;
                tmp = (tmp > 10) ? 10 : tmp;
                finalRes += tmp;
                //console.log('row_cells = '+row_cells.toString());
                //console.log('tmp = '+tmp);
            }
        }
        
        // столбцы
        if(col_cells.length > 2){
            for(var k = 0; k < col_cells.length - 1; k++){
                if(col_cells[k] > col_cells[k + 1]){
                    eval_array[2] += col_cells[k] - col_cells[k + 1];
                } 
                if(col_cells[k] < col_cells[k + 1]){
                    eval_array[3] += col_cells[k + 1] - col_cells[k];
                }
                if(col_cells[k] === col_cells[k + 1]){
                    megres++;
                }
            }
            if(!(eval_array[2] === 0 || eval_array[3] === 0)){
                tmp = Math.log2((eval_array[2] * eval_array[3]) / 1000);
                tmp = (tmp < 1) ? 1 : tmp;
                tmp = (tmp > 10) ? 10 : tmp;
                finalRes += tmp;
                //console.log('col_cells = '+col_cells.toString());
                //console.log('tmp = '+tmp);
            }
        }
    }
    
    //console.log('finalRes = '+finalRes);
    
    // Итоговый диапазон монотонности 0 - 80. Приведем его к общему для всех 0 - 100:
    finalRes *= 1.25;
    //console.log('finalRes *= 0.83 = '+finalRes);
    // инвертируем его. Теперь 0 - худшая оценка монотонности, 100 - лучшая.
    finalRes = 100 - finalRes;
    //console.log('100 - finalRes = '+finalRes);
    
    // возможные слияния, диапазон 0 - 15. Приведем его к общему для всех 0 - 100:
    megres *= 6.66;
    
    //console.log('megres = '+megres);
    
    return { monotonicity: finalRes, merges: megres };
}

// [4, 8, 32, 2, 16, 128, 128, 512, 2, 0, 8, 2, 0, 0, 2, 1024]